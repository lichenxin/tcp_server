package main

import (
	"net"
	"fmt"
	"os"
	"./protocol"
	"time"
)

func main() {

	tcp, err := net.ResolveTCPAddr("tcp4", "127.0.0.1:7981")
	if err != nil {
		fmt.Println("error:", err.Error())
		os.Exit(1)
	}

	conn, err := net.DialTCP("tcp", nil, tcp)
	if err != nil {
		fmt.Println("error:", err.Error())
		os.Exit(1)
	}

	fmt.Println("connect success")

	defer conn.Close()

	go protocol.Read(conn, func(data string) {
		fmt.Println("from server message: ", data)
	})

	for i := 0; i < 100; i++ {
		message := fmt.Sprintf("%d, hello word", i)
		fmt.Println("send message:", message)

		conn.Write(protocol.Packet([]byte(message)))
		time.Sleep(2 * time.Second)
	}
}